<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'WelcomeController')->name('Home');

Route::get('/Work', 'WorkController@index')->name('Work');
Route::get('/About', 'AboutController@index')->name('About');
Route::get('/Contact', 'ContactController@index')->name('Contact');
Route::get('/Hire', 'HireController@index')->name('Hire');
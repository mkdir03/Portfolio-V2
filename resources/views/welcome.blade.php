@extends('layouts/master')


@section('content')

@include('components/headerFooter')

<div class="perspective effect-rotate-left">
    <div class="container"><div class="outer-nav--return"></div>
      <div id="viewport" class="l-viewport">
        <div class="l-wrapper">

            @include('components/header')
            @include('components/sideNav')
            <ul class="l-main-content main-content">
                @include('partials/sectionIntro')
                @include('partials/sectionWork')
                @include('partials/sectionCareer')
                @include('partials/sectionContact')
                @include('partials/sectionHire')
            </ul>
        </div>
      </div>
    </div>
    @include('components/outerNav')
</div>

@endsection
<nav class="l-side-nav">
    <ul class="side-nav">
      <li class="is-active"><span>Home</span></li>
      <li><span><a href="{{route('Work')}}"></a>Works</span></li>
      <li><span><a href="{{route('About')}}"></a>About</span></li>
      <li><span><a href="{{route('Contact')}}"></a>Contact</span></li>
      <li><span><a href="{{route('Hire')}}"></a>Hire us</span></li>
    </ul>
  </nav>
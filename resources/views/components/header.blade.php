<header class="header">
    <a class="header--logo" href="#0">
      <img src="{{asset('img/logo.png')}}" alt="Global">
      <p>Global</p>
    </a>
    <button class="header--cta cta">Hire Us</button>
    <div class="header--nav-toggle">
      <span></span>
    </div>
  </header>